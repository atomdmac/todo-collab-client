import React from 'react';
import { Component } from 'react'
import './App.css';
import connection from './connection'

const uuid = require('uuid').v4

class App extends Component {
  constructor() {
    super()
    this.state = {
      items: []
    }

    this.handleAddNewTodo = this.handleAddNewTodo.bind(this)
    this.handleCompleteToggle = this.handleCompleteToggle.bind(this)
  }

  componentDidMount() {
    const self = this
    const queryDescriptor = {
      $sort: {
        completed: 1,
        label: 1
      }
    }
    const query = connection.createSubscribeQuery('todos', queryDescriptor)

    const update = () => {
      self.setState({
        ...self.state,
        items: query.results
      })
    }

    query.on('ready', update)
    query.on('changed', update)
  }

  handleAddNewTodo(label) {
    const doc = connection.get('todos', uuid())
    doc.create({
      label,
      completed: false
    })
  }

  handleCompleteToggle(itemId, completed) {
    const op = [{p: ['completed'], od: !completed, oi: completed}]
    connection
      .get('todos', itemId)
      .submitOp(op, (error) => {
        if (error) {
          throw error
        }
      })
  }

  render() {
    return (
      <div className="App">
        <TodoEditor
          handleSubmit={this.handleAddNewTodo}
        />
        <TodoList
          items={this.state.items}
          handleCompleteToggle={this.handleCompleteToggle}
        />
      </div>
    );
  }
}

const TodoEditor = ({label, handleSubmit}) => {
  const inputRef = React.createRef()
  const handleSubmitAndClear = () => {
    handleSubmit(inputRef.current.value)
    inputRef.current.value = ''
    inputRef.current.focus()
  }
  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleSubmitAndClear()
    }
  }

  return (
    <div className="todo-editor">
      <input
        ref={inputRef}
        type="text"
        value={label}
        onKeyPress={handleKeyPress}
      />
      <button 
        onClick={handleSubmitAndClear}>
        ➕
      </button>
    </div>
  )
}

const TodoList = ({items, handleCompleteToggle}) => (
  <ul className="todo-list">
    {items.map(item =>
      <TodoItem
        key={item.id}
        item={item}
        handleCompleteToggle={handleCompleteToggle}
      />
    )}
  </ul>
)

class TodoItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props
    }
  }

  componentDidMount() {
    this.props.item.subscribe()
    this.props.item.on('load', update)
    this.props.item.on('op', update)
    const self = this;
    function update() {
      self.forceUpdate()
    }
  }

  componentWillUnmount() {
    this.props.item.unsubscribe()
  }

  render() {
    const toggleComplete = (event) =>
      this.props.handleCompleteToggle(this.props.item.id, event.target.checked)
    const {
      item
    } = this.props

    return (
      <li className={item.data.completed ? 'todo-item completed': 'todo-item'}>
        <label>
          <input
            type="checkbox"
            checked={item.data.completed}
            onChange={toggleComplete}
          />
          {item.data.label}
        </label>
      </li>
    )
  }
}

export default App;
