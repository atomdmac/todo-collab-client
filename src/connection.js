const ShareDB = require('sharedb/lib/client')
const socket = new WebSocket(`ws://${window.location.hostname}:3001`)
const connection = new ShareDB.Connection(socket)
export default connection
